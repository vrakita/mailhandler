<?php

require './vendor/autoload.php';

use MailHandler\InboxHandler;


$config = [
	'imap'  	=> 'imap.yourmail.com',
    'port'  	=> '993',
    'username' 	=> 'username@yourmail.com',
    'password' 	=> 'secretstring'
];


try {

	$mail = InboxHandler::connect($config);
	$mail->load();

} catch(\Exception $e) {
	echo $e->getMessage();
	exit;
}

$messages = $mail->getMessages();

foreach($messages as $message) {
	echo 'ID:' . $message->getId();
	echo '<br>'; 
	echo 'From: ' . $message->getFrom();
	echo '<br>';
	echo 'Subject: ' . $message->getSubject();
	echo '<br>';
	echo 'New: ' . ($message->isNew() ? 'Yes' : 'No');
	echo '<br>';
	echo 'Date: ' . $message->getDate();
	echo '<br>';
	echo 'Message Plain: ' . '<br>' . $message->getPlainContent();
    echo '<br>';
    echo 'Message Html: ' . '<br>' . $message->getContent();
	echo '<hr>';

}

