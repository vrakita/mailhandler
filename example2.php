<?php

require './vendor/autoload.php';

use MailHandler\InboxHandler;
use MailParser\EmailParser;
use MailParser\ParserManager;

$config = [
    'imap'      => 'imap.yourmail.com',
    'port'      => '993',
    'username'  => 'username@yourmail.com',
    'password'  => 'secretstring'
];

try {

    echo '<b>connecting to imap server......</b>';
    echo "</br>";
    echo '</br>';

    $mail = InboxHandler::connect($config);

    $mail->load();

} catch(\Exception $e) {
    echo $e->getMessage();
    exit;
}

$messages = $mail->getMessages();

foreach($messages as $message) {

    try{

        echo '<b>Parsing mail From:  </b>' . $message->getFrom() . '</br>';
        echo '<b>Mail Subject:  </b>' . $message->getSubject() . '</br>';

        $parser = ParserManager::getParser($message);

        echo str_replace("\n", '</br>', json_encode(EmailParser::parse($parser, $message->getContent()), JSON_PRETTY_PRINT));

        echo '</br>';
        echo '</br>';

    }catch(\Exception $e){
        echo $e->getMessage() . '<br>'. '<br>';
    }

}






