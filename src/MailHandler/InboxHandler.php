<?php

namespace MailHandler;

use MailHandler\Contracts\Inbox;
use MailHandler\MessageHandler;

class InboxHandler implements Inbox {

	/**
	 * @var $imap source
	 */
	protected $imap;

	/**
	 * @var array ids of loaded messages
	 */
	protected $uids;

	/**
	 * @var array loaded messages
	 */
	protected $messages;

	protected function  __construct() {}

	/**
	 * Connect to IMAP protocol
	 * 
	 * @param  array $config IMAP credentials
	 * @return \MailHandler\InboxHandler
	 */
	public static function connect($config) {

		$inbox = new static;

		$inbox->imap = @imap_open('{' . $config['imap'] . ':' . $config['port'] . '/ssl}/INBOX', $config['username'] , $config['password']);
		imap_errors();
		imap_alerts();

		if( ! $inbox->imap) throw new \Exception('Connection could not be established');		

		return $inbox;

	}


	/**
	 * Load messages from IMAP
	 *
	 * @param int $limit
	 * @return void
	 */
	public function load($limit = 20) {		

		$this->getMessagesIds($limit);

		if(empty($this->uids)) return $this->messages = [];

		foreach($this->uids as $id) {

			$message 	= new MessageHandler;

			$msgNo 		= imap_msgno($this->imap, $id);

			$message->setHeader(imap_header($this->imap, $msgNo, FT_UID));

			$msgID 		= imap_uid($this->imap, $message->getHeader()->Msgno);
			
			$message->setContent($this->getBody($this->imap, $msgID));
			$message->setPlainContent($this->getBody($this->imap, $msgID, true));
			$message->setId($msgID);

			$this->messages[] = $message;

		}

	}



	protected function getMessagesIds($limit) {

		$numMessages = imap_num_msg($this->imap);

		if($limit > $numMessages) $limit = 1;
		else $limit = $numMessages - $limit + 1;

		for ($i = $numMessages; $i >= $limit; $i--) {
		    

		    $this->uids[] = imap_uid($this->imap, $i);

		}

	}



	protected function getPart($imap, $uid, $mimetype, $structure = false, $partNumber = false) {

	    if ( ! $structure) $structure = imap_fetchstructure($imap, $uid, FT_UID);
	    
	    if ($structure) {

	        if ($mimetype == $this->getMimeType($structure)) {
	            if ( ! $partNumber) $partNumber = 1;
	            
	            $text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);

                if ( $this->isISO_8859_1($structure) ){

                    switch ($structure->encoding) {
                        case 3: return utf8_encode(imap_base64($text));
                        case 4: return utf8_encode(imap_qprint($text));
                        default: return utf8_encode($text);
                    }

                }else{

                    switch ($structure->encoding) {
                        case 3: return imap_base64($text);
                        case 4: return imap_qprint($text);
                        default: return $text;
                    }

                }

	       }

	        // multipart 
	        if ($structure->type == 1) {
	            
	            foreach ($structure->parts as $index => $subStruct) {

	                $prefix = "";

	                if ($partNumber) $prefix = $partNumber . ".";

	                $data = $this->getPart($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));

	                if ($data) return $data;
	            }
	        }
	    }

	    return false;

	}

	protected function isISO_8859_1($structure){

	    if ( !isset($structure) || !isset($structure->parameters) ) return false;

        if( isset($structure->parameters[0]) && isset($structure->parameters[0]->value) && strtolower($structure->parameters[0]->value) === "iso-8859-1" ) return true;

	    return false;

    }


	protected function getMimeType($structure) {

	    $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

	    if ($structure->subtype) return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;

	    return "TEXT/PLAIN";
	}



	protected function getBody($imap, $uid, $plain = false) {


	    $body = $this->getPart($imap, $uid, "TEXT/HTML");
	    
	    if ($body == "") $body = $this->getPart($imap, $uid, "TEXT/PLAIN");

	    if($plain) return $this->getPart($imap, $uid, "TEXT/PLAIN");

	    return $body;
	}



	/**
	 * Return all loaded messages
	 *
	 * @return array $messages
	 */
	public function getMessages() {

		return $this->messages;

	}

	/**
	 * Delete messages from inbox by give uid arra
	 * 	
	 * @param  array  $ids
	 * @return void
	 */
	public function deleteMessage($ids = []) {

		if( empty($ids)) $ids = $this->uids;

		if( empty($ids)) return false;

		$deleted = 0;

		foreach ($ids as $id) {
			if(imap_delete($this->imap, $id, FT_UID)) $deleted++;
		}		

		return $deleted;

	}



	/**
	 * Return all unread messages
	 *
	 * @return array $messages
	 */
	public function getUnreadMessages() {

		$bucket = [];

		if( ! count($this->messages)) return false;

		foreach ($this->messages as $message) {

			if($message->isNew()) {

				$unread = new MessageHandler;
				$unread->setHeader(imap_header($this->imap, $message->getHeader()->Msgno));
				$unread->setContent($this->getBody($this->imap, $message->getHeader()->Msgno));

				$bucket[] = $unread;
			}			
			
		}

		return $bucket;

	}

	public function __destruct() {
		imap_expunge($this->imap);
		imap_close($this->imap);
	}

}