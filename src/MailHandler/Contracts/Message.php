<?php

namespace MailHandler\Contracts;

interface Message {


	/**
	 * Return body from message
	 *
	 * @return mixed $body
	 */
	public function getContent();

	/**
	 * Return plain text body from message
	 *
	 * @return mixed $body
	 */
	public function getPlainContent();

	/**
	 * Return date of message
	 *
	 * @return datetime $date
	 */
	public function getDate();

	/**
	 * Return subject of message
	 *
	 * @return string $subject
	 */
	public function getSubject();

	/**
	 * Return sender address of message
	 *
	 * @return string $from
	 */
	public function getFrom();

	/**
	 * Return message id
	 *
	 * @return mixed $id
	 */
	public function getId();


}