<?php

namespace MailHandler\Contracts;

interface InboxReceiver {

	/**
	 * Set header
	 *
	 * @param mixed $header
	 * @return void
	 */
	public function setHeader($header);

	/**
	 * Set content
	 * 
	 * @param mixed  $content 
	 * @param boolean $plain
	 */
	public function setContent($content);

	/**
	 * Set plain text content
	 * 
	 * @param mixed  $content 
	 * @param boolean $plain
	 */
	public function setPlainContent($content);

	/**
	 * Return header from message
	 *
	 * @return mixed $header
	 */
	public function getHeader();

}
