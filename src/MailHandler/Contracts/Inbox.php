<?php

namespace MailHandler\Contracts;

interface Inbox {

	/**
	 * Connect to IMAP protocol
	 * 
	 * @param  array $config IMAP credentials
	 * @return \MailHandler\InboxHandler
	 */
	public static function connect($config);

	/**
	 * Load messages from IMAP
	 *
	 * @param int $limit
	 * @return \MailHandler\InboxHandler
	 */
	public function load($limit = 20);

	/**
	 * Return all loaded messages
	 *
	 * @return array $messages
	 */
	public function getMessages();

	/**
	 * Return all unread messages
	 *
	 * @return array $messages
	 */
	public function getUnreadMessages();

	/**
	 * Delete messages from inbox by give uid arra
	 * 	
	 * @param  array  $ids
	 * @return void
	 */
	public function deleteMessage($ids = []);

}