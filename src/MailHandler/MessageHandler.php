<?php

namespace MailHandler;

use MailHandler\Contracts\Message;
use MailHandler\Contracts\InboxReceiver;

class MessageHandler implements Message, InboxReceiver {

	/**
	 * @var mixed $id
	 */
	protected $id;

	/**
	 * @var mixed $header
	 */
	protected $header;

	/**
	 * @var mixed $body
	 */
	protected $content;

	/**
	 * @var mixed $body
	 */
	protected $plainContent;

	/**
	 * @var datetime $date
	 */
	protected $date;

	/**
	 * @var datetime $subject
	 */
	protected $subject;

	/**
	 * @var datetime $from
	 */
	protected $from;

	/**
	 * Set header
	 *
	 * @param mixed $header
	 * @return void
	 */
	public function setHeader($header) {
		$this->header 	= $header;
		$this->date 	= $this->header->date;
		$this->subject 	= $this->header->subject;
		$this->from 	= implode('@', [$this->header->from[0]->mailbox, $this->header->from[0]->host]);
	}

	/**
	 * Return message id
	 *
	 * @return mixed $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Return message id
	 *
	 * @param mixed $id
	 * @return mixed $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * Set content
	 *
	 * @param mixed $content
	 * @return void
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Set content
	 *
	 * @param mixed $content
	 * @return void
	 */
	public function setPlainContent($content) {
		$this->plainContent = $content;
	}

	/**
	 * Return header from message
	 *
	 * @return mixed $header
	 */
	public function getHeader() {
		return $this->header;
	}

	/**
	 * Return body from message
	 *
	 * @return mixed $body
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Return plain text body from message
	 *
	 * @return mixed $body
	 */
	public function getPlainContent() {
		return $this->plainContent;
	}

	/**
	 * Return date of message
	 *
	 * @return datetime $date
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * Return subject of message
	 *
	 * @return string $subject
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * Return sender address of message
	 *
	 * @return string $from
	 */
	public function getFrom() {
		return $this->from;
	}

	/**
	 * Return is message read
	 *
	 * @return bool
	 */
	public function isNew() {
		return strtolower($this->header->Recent) == 'n' || strtolower($this->header->Unseen) == 'u';
	}

}

