<?php

/**
 * Created by PhpStorm.
 * User: Arnold
 * Date: 7/19/2016
 * Time: 2:57 PM
 */
namespace MailParser;

use \DateTime;

class EmailParser {

    /**
     * Prevents class to be manually instantiated
     *
     */
    protected function __construct(){}


    /**
     * @param $parserHandler
     * @param $content
     * @return mixed
     * @throws \Exception
     */

    public static function parse($parserHandler, $content)
    {

        try {

            return $parserHandler->parse($content);
        } catch(\MailParser\Exceptions\TNTContentException $e) {

            throw new \MailParser\Exceptions\TNTContentException($e->getMessage());

        } catch (\Exception $e) {

            throw new \Exception($e->getMessage());

        }
    }

   

}