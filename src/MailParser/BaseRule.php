<?php namespace MailParser;

use MailHandler\Contracts\Message;

abstract class BaseRule {

	/**
	 * Returns parser for given message
	 *
	 * @param \MailHandler\Contracts\Message
	 * @return \MailParser\BaseParser|false
	 */
	public static abstract function run(Message $message, $settings);

}