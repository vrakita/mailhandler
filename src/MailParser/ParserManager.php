<?php namespace MailParser;

use MailHandler\Contracts\Message;
use MailParser\Rules\Rules;

abstract class ParserManager {


    public static function getParser(Message $message, $rules = [], $settings = false) {

        foreach(Rules::getRules($rules) as $rule) {

            if($parser = $rule::run($message, $settings)) return new $parser;

        }

        throw new \MailParser\Exceptions\ParserNotFoundException('Parser cannot be found based on defined rules');       
        
    }



}