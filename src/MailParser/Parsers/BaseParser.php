<?php namespace MailParser\Parsers;

abstract class BaseParser {

    protected function splitToRows($content) {

        return $this->cleanArray(explode("\n", $content));

    }

    /**
     * Removes empty strings from array
     * 
     * @param $array
     * @param bool $removeCharacter
     * @return array
     */
    protected function cleanArray($array, $removeCharacter = false) {

        if ($removeCharacter) {

            $array = array_map(function ($item) use ($removeCharacter) {

                return trim(str_replace($removeCharacter, "", $item));

            }, $array);

        }

        return array_filter($array);

    }

}