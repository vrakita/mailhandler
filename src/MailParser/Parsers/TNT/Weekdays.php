<?php namespace MailParser\Parsers\TNT;

use MailParser\Parsers\TNT\TNTParser;
use MailParser\Exceptions\TNTContentException;

class Weekdays extends TNTParser {

    /**
     * Parse array of email body row data
     * @param $content
     * @return array
     * @throws \Exception
     */
    public function parse($content) {

        $row = $this->splitToRows($content);

        if ( ! is_array($row)) throw new \Exception('Parsed content not valid');

        $puTimeAdded = false;

        foreach ($row as $value) {

            $pair = explode(":", $value, 2);

            $pair = $this->cleanArray($pair, ['=20','=']);

            if (isset($pair[0])) {

                if (count($pair) != 2) continue;

                if (strtolower($pair[0]) == 'frachtbriefnummer') {

                    $pair[0] = 'con';

                    $pair[1] = preg_replace('/[^A-Za-z0-9]/', '', $pair[1]);

                } else if (strtolower($pair[0]) == 'referenznummer') {

                    $pair[0] = 'time_of_birth';

                    $pair[1] = $this->cleanTimeOfBirthFormat($pair[1]);

                } else if (strtolower($pair[0]) == 'abholtag') {

                    $pair[0] = 'pick_up_date';

                    $pair[1] = $this->cleanDateFormat($pair[1]);

                } else if (strtolower($pair[0]) == 'abholzeiten') {

                    $timeInterval = trim($pair[1]);

                    if ( isset($timeInterval) && isset($this->parsedContent['pick_up_date']) ){

                        $this->parsedContent['pick_up_date'] = $this->parsedContent['pick_up_date'] . " ". $this->cleanTimeFormat($this->extractPickUpTime($timeInterval));

                        $puTimeAdded = true;

                    }

                    continue;

                }else continue;

                $this->parsedContent[strtolower($pair[0])] = $pair[1];

            }

            if(isset($this->parsedContent['con'], $this->parsedContent['time_of_birth'], $this->parsedContent['pick_up_date']) && $puTimeAdded) break;

        }

        try {

            $this->validContent();

        } catch(\Exception $e) {

            throw new TNTContentException($e->getMessage());

        }

        return $this->parsedContent;

    }

}