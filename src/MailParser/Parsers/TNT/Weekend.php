<?php namespace MailParser\Parsers\TNT;

use MailParser\Parsers\TNT\TNTParser;
use MailParser\Exceptions\TNTContentException;

class Weekend extends TNTParser {

    /**
     * Parse array of email body row data
     * @param $content
     * @return array
     * @throws \Exception
     */

    public function parse($content) {


        if($con = $this->getPartOfString($content, 'frachtbriefnummer')) {

            $this->parsedContent['con'] = $con;

        }

        if($timeOfBirth = $this->getPartOfString($content, "Datum /", 100)) {

            $this->parsedContent['time_of_birth'] = $this->makeOneSpaceBetween($timeOfBirth);
            $this->parsedContent['time_of_birth'] = str_replace(".", "-", $this->parsedContent['time_of_birth']);
            $this->parsedContent['time_of_birth'] = date('Y-m-d H:i:s', strtotime($this->parsedContent['time_of_birth']));

        }

        if($pickupDate = $this->getPartOfString($content, "Abholtag", 100)) {

            $this->parsedContent['pick_up_date'] = $this->makeOneSpaceBetween($pickupDate);
            $this->parsedContent['pick_up_date'] = str_replace(".", "-", $this->parsedContent['pick_up_date']);
            $this->parsedContent['pick_up_date'] = date('Y-m-d H:i:s', strtotime($this->parsedContent['pick_up_date']));

        }

        try {

            $this->validContent();

        } catch(\Exception $e) {

            throw new TNTContentException($e->getMessage());

        }

        return $this->parsedContent;

    }

}