<?php namespace MailParser\Parsers\TNT;

use MailParser\Parsers\BaseParser;
use MailParser\Exceptions\TNTContentException;

abstract class TNTParser extends BaseParser {

    protected $parsedContent = [];

	/**
     * Parse array of email body row data
     *
     * @throws \Exception
     * @return void
     */
    abstract public function parse($content);

    protected function validContent() {

        if ( ! isset($this->parsedContent['con']) || ! $this->isConValid($this->parsedContent['con']))
            throw new \Exception('CON not valid');
        if ( ! isset($this->parsedContent['time_of_birth']) || ! $this->isDateValid($this->parsedContent['time_of_birth'], 'Y-m-d H:i:s')) {
            throw new TNTContentException('Time of birth (' . $this->parsedContent['time_of_birth'] . ') not valid for con:' . $this->parsedContent['con']);
        }
        if ( ! isset($this->parsedContent['pick_up_date']) || ! $this->isDateValid($this->parsedContent['pick_up_date'], 'Y-m-d H:i:s')) {
            throw new TNTContentException('Pick up date (' . $this->parsedContent['pick_up_date'] . ') not valid for con:' . $this->parsedContent['con']);
        }
        return true;

    }

    /**
     * Removes special characters, multiple empty spaces, changes to Y.m.d H:i format
     *
     * @return string
     */
    protected function cleanTimeOfBirthFormat($dateTime, $formatTo = 'Y-m-d H:i:s')
    {
        $dateTime = trim($dateTime);

        $dateTime = str_replace(['     ','    ', '   ', '  '], ' ', $dateTime);

        $dateTimeArray = explode(" ", $dateTime, 2);

        if ( count($dateTimeArray) < 2) return $dateTime;

        $dateTime = $this->cleanDateFormat( $dateTimeArray[0] ) . " " . $this->cleanTimeFormat($dateTimeArray[1]);

        if ($this->isDateValid($dateTime, "d.m.Y H:i:s") && $dateTime != 'Leerbox') {

            $time = strtotime($dateTime);

            if($time) return date($formatTo, $time);

        }

        return $dateTime;
    }

    /**
     * cleans the date, and changes from format d.m.Y, to format Y.m.d, adds missing 0 and 20
     *
     * @param $date
     * @return mixed|string
     */
    protected function cleanDateFormat($date){

        $date = trim($date);

        $date = str_replace('.', '-', $date);$date = str_replace('/', '-', $date);

        $dateArray = explode("-", $date);

        if ( count($dateArray) < 3) return $date;

        if ( strlen($dateArray[0]) < 2 ){ $dateArray[0] = "0" . $dateArray[0]; }

        if ( strlen($dateArray[1]) < 2 ){ $dateArray[1] = "0" . $dateArray[1]; }

        if ( strlen($dateArray[2]) < 3 ){ $dateArray[2] = "20" . $dateArray[2]; }

        $date = $dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0];

        return $date;

    }

    /**
     * cleans h.m to h:m and changes to h:m:s format
     *
     * @param $time
     * @return mixed|string
     */

    protected function cleanTimeFormat($time){

        $time = trim($time);
        $time = str_replace('.', ':', $time);

        $timeArray = explode(':', $time);

        if (count($timeArray) < 2) return $time;

        if ( strlen($timeArray[0]) < 2 ){ $timeArray[0] = "0" . $timeArray[0]; }

        if ( strlen($timeArray[1]) < 2 ){ $timeArray[1] = "0" . $timeArray[1]; }

        return $timeArray[0] . ':' . $timeArray[1] . ':00';

    }

    /**
     * Checks is the date in valid format
     *
     * @return boolean
     */
    protected function isDateValid($date, $format) {   

        if ($date == '1970-01-01 01:00:00') return false;

        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Checks is the date in valid format, or Leerbox
     *
     * @return boolean
     */
    protected function isTimeOfBirthValid($date, $format = 'Y-m-d H:i:s') {

        $time = strtotime($date);

        if( ! $time) return false;

        return date($format, $time);

    }

    /**
     * Checks is the con in valid
     *
     * @return boolean
     */
    protected function isConValid($con) {

        return strlen($con) <= 50 && strlen($con) > 0;

    }

    /**
     * Extractsteh pickup time from time, if time is interval, returns second value
     *
     * @param $time
     */

    protected function extractPickUpTime($timeInterval){

        $timeInterval =  preg_replace('/[^0-9\.\-\ \:]/', '', $timeInterval);

        $timeArray = explode("-", $timeInterval);

        if ( count($timeArray) == 1 ) return trim(str_replace(['-','.'], ':', $timeArray[0]));

        if ( count($timeArray) == 2) return trim(str_replace(['-','.'], ':', $timeArray[1]));

        return $timeInterval;

    }

    /**
     * Parse part of string
     *
     * @param string $string
     * @param string $needle
     * @param int $length
     *
     * @return string|false
     */
    protected function getPartOfString($string, $needle, $length = 20) {

        $string = strip_tags($string);

        $string = trim($string);

        $string = str_replace('\x0D', '', $string);

        $string = strtolower($string);

        $start = strpos($string, strtolower($needle)) + strlen($needle);

        if($start === false) return false;

        $length += strlen($needle);

        $result = substr($string, $start, $length);

        $result = preg_replace("/[^0-9.,-: ]/", "", $result);

        $result = trim($result);

        $result = str_replace([" /", "/ ", " / "], "", $result);

        return trim($result);


    }

    protected function makeOneSpaceBetween($string) {

        if(count($parts = explode(" ", $string)) > 1) {

            $parts = array_filter($parts);

            $parts = array_values($parts);

            if(count($parts) >= 2) {

                $string = $parts[0] . ' ' . $parts[1];

            }

        } else {

            $year = date('Y');

            if(strpos($string, $year) !== false) {

                $string = str_replace($year, $year . " ", $string);

            }

        }

        return $string;
    }

}