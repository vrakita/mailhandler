<?php namespace MailParser\Parsers\Seracell;

use MailParser\Parsers\BaseParser;

abstract class SeracellParser extends BaseParser {

    /**
     *  Parse array of email body row data
     * @param $content
     * @return array
     * @throws \Exception
     */
    public function parse($content) {

        $parsedContent = [];

        $content = $this->cleanArray($this->splitToRows($content));

        if (!is_array($content) || count($content) < 3 || count($content) > 15) throw new \Exception('Parsed content not valid');

        foreach ($content as $value) {

            $pair = explode(":", $value, 2);

            $pair = $this->cleanArray($pair, ['<', '>']);

            if (count($pair) != 2) continue;

            $pair[0] = str_replace(" ", "_", $pair[0]);

            $parsedContent[strtolower($pair[0])] = $pair[1];

        }

        if( $this->validContent($parsedContent)) return $parsedContent;;

    }

    /**
     * validates the emails parsedContent
     * @param $parsedContent
     * @return mixed
     */
    abstract protected function validContent($parsedContent);

    /**
     * checks is the data in timestamp of timestamp format
     * @param $parsedContent
     * @return bool
     */
    protected function isTimestampValid($parsedContent){
        if (
            (string) (int) $parsedContent['timestamp'] === $parsedContent['timestamp'] &&
            ($parsedContent['timestamp'] <= PHP_INT_MAX) &&
            ($parsedContent['timestamp'] >= ~PHP_INT_MAX)
        ) return true;

        return false;
    }

}