<?php namespace MailParser\Parsers\Seracell;

use MailParser\Parsers\Seracell\SeracellParser;

class BoxArrived extends SeracellParser {

    /**
     * validates the emails parsedContent
     * @param $parsedContent
     * @return bool
     * @throws \Exception
     */

    protected function validContent($parsedContent) {

        $errorMessage='';

        if (    ! isset($parsedContent['contract_id']) ||
            strlen($parsedContent['contract_id']) > 50 ||
            strlen($parsedContent['contract_id']) < 1
        ) $errorMessage .= ', contract_id Not valid';

        if (    ! isset($parsedContent['box_id']) ||
            strlen($parsedContent['box_id']) > 50 ||
            strlen($parsedContent['box_id']) < 1
        ) $errorMessage .= ', box_id Not valid';

        if (    ! isset($parsedContent['timestamp']) ||
            ! $this->isTimestampValid($parsedContent)
        ) $errorMessage .= ', timestamp Not valid';

        if ($errorMessage != '') throw new \Exception('Content cannot be parsed' . $errorMessage . '.');

        return true;

    }

}