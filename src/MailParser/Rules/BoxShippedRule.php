<?php namespace MailParser\Rules;

use MailParser\BaseRule;
use MailHandler\Contracts\Message;

abstract class BoxShippedRule extends BaseRule {

	/**
	 * Returns parser for given message
	 *
	 * @param \MailHandler\Contracts\Message
	 * @param mixed $settings
	 * @return \MailParser\BaseParser|false
	 */
	public static function run(Message $message, $settings) {

		$sender = explode('@', $message->getFrom());

		$address = isset($settings['seracell_address']) ? $settings['seracell_address'] : 'seracell.de';

		if(trim(strtolower($sender[1])) !== $address) return false;

		if(strpos(strtolower($message->getSubject()), 'shipped') !== false  && 
			strpos(strtolower($message->getContent()), 'imei') !== false) return \MailParser\Parsers\Seracell\BoxShipped::class;

		return false;

	}

}