<?php namespace MailParser\Rules;

use MailParser\BaseRule;
use MailHandler\Contracts\Message;

abstract class TNTWeekdaysRule extends BaseRule {

	/**
	 * Returns parser for given message
	 *
	 * @param \MailHandler\Contracts\Message
	 * @param mixed $settings
	 * @return \MailParser\BaseParser|false
	 */
	public static function run(Message $message, $settings) {

		$sender = explode('@', $message->getFrom());

		$address = isset($settings['courier_email']) ? $settings['courier_email'] : 'tnt.de';

		if(trim(strtolower($sender[1])) !== $address) return false;

		return strpos(strtolower($message->getContent()), 'bernahmequittung') !== false ? \MailParser\Parsers\TNT\Weekdays::class : false;

	}

}