<?php namespace MailParser\Rules;

abstract class Rules {


	public static function getRules($rules = []) {

		return ! empty($rules) ? $rules : [

			/**
			 * Production ready rules
			 *
			 */
			\MailParser\Rules\BoxArrivedRule::class,
			\MailParser\Rules\BoxShippedRule::class,
			\MailParser\Rules\TNTWeekdaysRule::class,
			\MailParser\Rules\TNTWeekendRule::class,

		];

	}

}