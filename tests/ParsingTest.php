<?php 

use MailHandler\MessageHandler;

class ParsingTest extends PHPUnit_Framework_TestCase {

    protected $dateSeparatedWithForwardSlash;
    protected $tntweekend20161102;
    protected $tntweekend20161116;
    protected $wrongWeekendDate;

	public function setUp() {

        $this->dateSeparatedWithForwardSlash    = file_get_contents('./tests/messages/failed_message_2016_10_28.txt');
        $this->tntweekend20161102				= file_get_contents('./tests/messages/Weekend-2016-11-02.txt');
        $this->tntweekend20161116               = file_get_contents('./tests/messages/Failed-Weekend-16-11-2016.txt');
        $this->wrongWeekendDate                 = file_get_contents('./tests/messages/Wrong-Weekend-Date.txt');

	}



    /**
     * @test
     * @expectedException \MailParser\Exceptions\TNTContentException
     */
    function test_custom_exception_for_wrong_date() {
        
        $content = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->wrongWeekendDate);        

    }


    /**
     * @test
     */
	function test_is_forward_slash_date_valid_ddmmyyyhis() {

        $content = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekdays, $this->dateSeparatedWithForwardSlash);
        $this->assertTrue(count($content) > 0);

	}

	/**
	 * @test
	 */
	function test_tnt_weekend_20161102() {
		$content = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161102);
        $this->assertTrue(count($content) > 0);
	}

    /**
     * @test
     */
    function test_con_number_found() {
        $content1 = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161102);
        $this->assertTrue(isset($content1['con']));

        $content2 = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161116);
        $this->assertTrue(isset($content2['con']));
    }

    /**
     * @test
     */
     function test_pickup_date() {
        $content = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161102);
        $this->assertTrue(isset($content['pick_up_date']));

        $content2 = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161116);
        $this->assertTrue(isset($content2['pick_up_date']));
    }

    /**
     * @test
     */
     function test_time_of_birth() {
        $content = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161102);
        $this->assertTrue(isset($content['time_of_birth']));

        $content2 = \MailParser\EmailParser::parse(new \MailParser\Parsers\TNT\Weekend, $this->tntweekend20161116);
        $this->assertTrue(isset($content2['time_of_birth']));
    }
}