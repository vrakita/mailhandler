<?php 

use MailHandler\MessageHandler;

include './tests/messages/FakeMessageObject.php';
include './tests/messages/FakeRuleObject.php';

class GetParserTest extends PHPUnit_Framework_TestCase {

	protected $fakeMessage;
	protected $tntWeekdays;
	protected $tntWeekend;
	protected $boxArrived;
	protected $boxShipped;
	protected $falseBoxArrived;
    protected $tntWeekendWrongDate;
    protected $dateSeparatedWithForwardSlash;

	public function setUp() {

		$this->fakeMessage 						= getFakeMessage();
		$this->tntWeekdays 						= getTntWeekdaysMessage();
		$this->boxArrived						= getBoxArrivedMessage();
		$this->boxShipped 						= getBoxShippedMessage();
		$this->falseBoxArrived					= getFalseBoxArrived();
		$this->falseBoxShipped					= getFalseBoxShipped();
		$this->tntWeekend 						= getTntWeekendMessage();
        $this->tntWeekendWrongDate 				= getTntWeekendMessageWrongDate();
        $this->dateSeparatedWithForwardSlash    = file_get_contents('./tests/messages/failed_message_2016_10_28.txt');

	}



	/**
	 * @test
	 */
	function test_receiving_box_arrived_instance() {

		$parser = \MailParser\ParserManager::getParser($this->boxArrived);

		$this->assertInstanceOf(\MailParser\Parsers\Seracell\BoxArrived::class, $parser);

	}

	/**
	 * @test
	 * @expectedException \MailParser\Exceptions\ParserNotFoundException
	 */
	function test_not_receiving_box_arrived_instance() {

		$parser = \MailParser\ParserManager::getParser($this->falseBoxArrived);

	}

	/**
	 * @test
	 * @expectedException \MailParser\Exceptions\ParserNotFoundException
	 */
	function test_not_receiving_box_shipped_instance() {

		$parser = \MailParser\ParserManager::getParser($this->falseBoxShipped);

	}

	/**
	 * @test
	 */
	function test_receiving_box_shipped_instance() {

		$parser = \MailParser\ParserManager::getParser($this->boxShipped);

		$this->assertInstanceOf(\MailParser\Parsers\Seracell\BoxShipped::class, $parser);

	}

	/**
	 * @test
	 */
	function test_receiving_tnt_weekdays_instance() {

		$parser = \MailParser\ParserManager::getParser($this->tntWeekdays);

		$this->assertInstanceOf(\MailParser\Parsers\TNT\Weekdays::class, $parser);
		$this->assertFalse(get_class($parser) === \MailParser\Parsers\TNT\Weekend::class);

	}

	/**
	 * @test
	 */
	function test_receiving_tnt_weekend_instance() {

		$parser = \MailParser\ParserManager::getParser($this->tntWeekend);

		$this->assertInstanceOf(\MailParser\Parsers\TNT\Weekend::class, $parser);
		$this->assertFalse(get_class($parser) === \MailParser\Parsers\TNT\Weekdays::class);

	}

	/**
	 * @test
	 */
	function test_receiving_custom_class_from_custom_rule_instance() {

		$parser = \MailParser\ParserManager::getParser($this->boxArrived, [FakeRuleObject::class]);

		$this->assertInstanceOf(stdClass::class, $parser);	

	}

}