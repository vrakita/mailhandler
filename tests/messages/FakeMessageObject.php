<?php

use MailHandler\MessageHandler;



function getFakeMessage() {
	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test shipped';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'vrakita'; 
	$header->from[0]->host = 'yahoo.com';

	$message = new MessageHandler;
	$message->setContent('Hello world');
	$message->setHeader($header);

	return $message;
}

function getBoxArrivedMessage() {
	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test delivered';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'vrakita'; 
	$header->from[0]->host = 'seracell.de';

	$message = new MessageHandler;
	$message->setContent('Hello world');
	$message->setHeader($header);

	return $message;
}

function getBoxShippedMessage() {
	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test shipped';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'vrakita'; 
	$header->from[0]->host = 'seracell.de';

	$message = new MessageHandler;
	$message->setContent('Hello world ImEi');
	$message->setHeader($header);

	return $message;
}

function getFalseBoxArrived() {

	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test arried';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'vrakita'; 
	$header->from[0]->host = 'seracell.de';

	$message = new MessageHandler;
	$message->setContent('Hello world');
	$message->setHeader($header);

	return $message;

}

function getFalseBoxShipped() {

	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test shipped';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'vrakita'; 
	$header->from[0]->host = 'seracell.de';

	$message = new MessageHandler;
	// Missin IMEI
	$message->setContent('Hello world');
	$message->setHeader($header);

	return $message;

}


function getTntWeekdaysMessage() {

	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test shipped';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'something'; 
	$header->from[0]->host = 'tnt.de';

	$message = new MessageHandler;
	$message->setContent('something dcbernahmequittung...');
	$message->setHeader($header);

	return $message;

}

function getTntWeekendMessage() {

	$header = new stdClass;
	$header->date 		= date('Y-m-d H:i:s');
	$header->subject 	= 'Test shipped';
	$header->from[0] = new stdClass;
	$header->from[0]->mailbox = 'something'; 
	$header->from[0]->host = 'tnt.de';

	$message = new MessageHandler;
	$message->setContent('anmeldung something seracell anmeldung...');
	$message->setHeader($header);

	return $message;

}

function getTntWeekendMessageWrongDate() {

    $header = new stdClass;
    $header->date 		= '04-ta-2016  13:10';
    $header->subject 	= 'Test shipped';
    $header->from[0] = new stdClass;
    $header->from[0]->mailbox = 'something';
    $header->from[0]->host = 'tnt.de';

    $message = new MessageHandler;
    $message->setContent('anmeldung something seracell anmeldung...');
    $message->setHeader($header);

    return $message;

}

function getForwardSlashSeparatedDateYMDHIS() {
	$header = new stdClass;
    $header->date 		= date('Y/m/d H:i:s');
    $header->subject 	= 'Test shipped';
    $header->from[0] = new stdClass;
    $header->from[0]->mailbox = 'something';
    $header->from[0]->host = 'tnt.de';

    $message = new MessageHandler;
    $message->setContent('anmeldung something seracell anmeldung...');
    $message->setHeader($header);

    return $message;
}