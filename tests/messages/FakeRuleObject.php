<?php

use MailParser\BaseRule;
use MailHandler\Contracts\Message;

class FakeRuleObject extends BaseRule {

	/**
	 * Returns parser for given message
	 *
	 * @param \MailHandler\Contracts\Message
	 * @return \MailParser\BaseParser|false
	 */
	public static function run(Message $message, $settings = false) {

		$sender = explode('@', $message->getFrom());

		$address = isset($settings['seracell_address']) ? $settings['seracell_address'] : 'seracell.de';

		if(trim(strtolower($sender[1])) !== $address) return false;

		if(strpos(strtolower($message->getSubject()), 'delivered') !== false &&
			strpos(strtolower($message->getContent()), 'imei') === false) return stdClass::class; 
		return false;

	}

}